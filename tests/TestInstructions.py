import glob
from unittest import TestCase
import tkinter as tk

from src.interfaces.Instructions import Instructions


class TestGame(TestCase):
    def setUp(self):
        root = tk.Tk()
        self.instructions = Instructions(root)

    def test_start(self):
        files = glob.glob('./instructions/*')
        self.assertIsNotNone(files, "Vuoto")