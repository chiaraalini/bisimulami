import glob
from unittest import TestCase

from src.GameManager import Game


class TestGame(TestCase):
    def setUp(self):
        self.game = Game()

    def test_start(self):
        self.game.start()
        files = glob.glob('./output/*')
        self.assertIsNotNone(files, "Vuoto")

    def test_new_move(self):
        self.assertEqual(self.game.new_move('a', 'p', 'p', 1), 0)
        self.assertEqual(self.game.new_move('d', 'q', 'q', 1), 0)
        self.assertEqual(self.game.new_move('a', 'p', 'q1', 1), -1)
        self.assertEqual(self.game.new_move('d', 'q', 'p1', 1), -1)
        self.assertEqual(self.game.new_move('a', 'p1', 'q', 1), -2)
        self.assertEqual(self.game.new_move('d', 'q1', 'p', 1), -2)
        self.assertNotEqual(self.game.new_move('a', 'p1', 'p1', 1), 1)

    def test_color_only_one_node(self):
        self.game.color_only_one_node('p1', 1)
        i = 0
        while i < len(self.game.text):
            # If node is in the graph
            if 'red' in self.game.text[i]:
                self.assertIn('p1', self.game.text[i], "Ok")
            i += 1

    def test_find_nodes(self):
        self.assertEqual(self.game.find_nodes('a', 'p1', 's21'), (1, 0, 0))
        self.assertEqual(self.game.find_nodes('a', 's21', 'p1'), (0, 1, 0))
        self.assertEqual(self.game.find_nodes('a', 'p1', 'p2'), (1, 1, 1))

    def test_set_label(self):
        self.game.find_nodes('a', 'p1', 'p2')
        self.assertIsNotNone(self.game.label_strk)
        self.assertEqual(self.game.label_def, '')

    def test_check_nodes(self):
        self.assertEqual(self.game.check_nodes('a', 'p1', 'p2', 1, 0, 0, 0), 0)
        self.assertEqual(self.game.check_nodes('a', 'p1', 'p2', 1, 0, 1, 0), -1)
        self.assertEqual(self.game.check_nodes('d', 'p1', 'p2', 1, 1, 0, 0), -2)
        self.assertEqual(self.game.check_nodes('d', 'p1', 'p2', 1, 1, 1, 0), -3)
        self.assertNotEqual(self.game.check_nodes('d', 'p1', 'p2', 1, 1, 1, 0), -4)
        self.assertNotEqual(self.game.check_nodes('a', 'p1', 'p2', 1, 1, 1, 0), -4)

    def test_clean_specific_code(self):
        self.game.find_nodes('a', 'p1', 'p2')
        self.game.clean_specific_code('p1', 'p2')
        i = 0
        while i < len(self.game.text):
            # If node is in the graph
            if 'p1' in self.game.text[i]:
                self.assertNotIn('red', self.game.text[i], "Ok")
            i += 1

    def test_clean_code(self):
        self.game.find_nodes('a', 'p1', 'p2')
        self.game.clean_code()
        i = 0
        while i < len(self.game.text):
            # If node is in the graph
            if 'p1' in self.game.text[i]:
                self.assertNotIn('red', self.game.text[i], "Ok")
                self.assertNotIn('green', self.game.text[i], "Ok")
                self.assertIn('white', self.game.text[i], "Ok")
                break
            i += 1

    def test_view_graph(self):
        self.game.view_graph('p1', 1)
        files = glob.glob('./output/*')
        self.assertIsNotNone(files, "Vuoto")
