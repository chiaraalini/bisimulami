# Bisimulami!

## Componenti del gruppo:

* Alini Chiara - 861204
* Costantino Chantal - 860621
* Fedel Valeria - 859984
* Mervic Francesco - 816871

## Descrizione progetto:
L'applicazione "Bisimulami!", in accordo con il laboratorio del DISCo MC3, verrà utilizzata nell'erogazione dell'insegnamento di Modelli della
concorrenza. Essa darà la possibilità ai giovani studenti del primo anno della magistrale in Informatica di giocare al "Gioco della bisimulazione".
Gli studenti potranno affrontarsi, scegliendo tra il ruolo di attaccante e difensore, sia sul web sia utilizzando il medesimo calcolatore. In base
ai risultati delle partite, gli verrà assegnato un punteggio che potrà essere visualizzato dai futuri avversari e che verrà utilizzato per stilare
delle classifiche. Sarà anche possibile giocare contro un avversario virtuale (comunemente chiamato: "Il computer") selezionando il livello di
difficoltà. Inoltre, sarà disponibile un "percorso" di partite (da giocarsi contro "il computer"), volto ad istruire il giocatore sulle strategie
vincenti. Infine, l'applicazione permetterà di salvare le partite (per concluderle successivamente) o per "riviverle" tramite la funzione:
"Replay", che visualizzerà la successione di mosse di attaccante e difensore.
