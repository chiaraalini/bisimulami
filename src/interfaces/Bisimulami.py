import tkinter as tk
from PIL import ImageTk
from PIL import Image
from src.GameManager import Game
from src.interfaces.Instructions import Instructions


class Bisimulami:

    def __init__(self, master,
                 player,
                 node_striker_src,
                 node_striker_dst,
                 node_defender_src,
                 node_defender_dst,
                 number_sx,
                 number_dx,
                 number_p,
                 number_q):
        """
        To initialize Biimulami instance
        :param master: tk instance
        :param player: striker or defender
        :param node_striker_src: source node for striker
        :param node_striker_dst: destination node for striker
        :param node_defender_src: source node for striker
        :param node_defender_dst: destination node for striker
        :param number_sx: counter for striker
        :param number_dx: counter for defender
        :param number_p: counter for the P graph
        :param number_q: counter for Q graph
        """

        self.master = master
        self.master.title("Bisimulami!")
        self.master.geometry('' '+300+5')
        self.master.configure(background='white')
        self.master.resizable(False, False)

        # Match image counter
        self.number_sx = number_sx
        self.number_dx = number_dx
        self.number_p = number_p
        self.number_q = number_q

        self.node_striker_src = node_striker_src
        self.node_striker_dst = node_striker_dst
        self.node_defender_src = node_defender_src
        self.node_defender_dst = node_defender_dst
        self.player = player
        self.check_switch_player = 'a'
        first_exception = 0

        # Frame to hold images
        self.master.frame_sx = tk.Frame(self.master, bd=0, background="white")
        self.master.frame_sx.grid(row=1, column=0, padx=3, pady=5)
        self.master.frame_dx = tk.Frame(self.master)
        self.master.frame_dx.grid(row=1, column=1, padx=3, pady=5)

        # Frame Instruction
        self.frame = tk.Frame(self.master, bd=1, cursor='arrow', relief="ridge")
        self.butnew("?", Instructions)
        self.frame.grid(row=0, columnspan=2, sticky='W')
        self.frame.configure(background='white')

        # If it's first time I've played
        if self.number_sx == 1 and self.number_dx == 1:
            # If it isn't a saved new match
            if self.player == '':
                self.my_game = Game()
            # If no match are found
            elif self.player == 'e':
                self.my_game = Game()
                first_exception = 1
            else:
                self.my_game = Game(self.player)
            self.player = ''
            self.id = self.my_game.dbctrl.id
            self.view_start_image("../output/output_p0_graph.png", "../output/output_q0_graph.png")

        else:
            self.my_game = Game(0)
            self.id = self.my_game.dbctrl.id
            var = self.my_game.new_move('a', self.node_striker_src, self.node_striker_dst, self.number_sx)
            var = self.my_game.new_move('d', self.node_defender_src, self.node_defender_dst, self.number_dx)
            path_sx = '../output/output_p{}_graph.png'.format(self.number_sx)
            path_dx = '../output/output_q{}_graph.png'.format(self.number_dx)
            self.my_game.view_graph(self.node_striker_src, self.number_sx)
            self.my_game.view_graph(self.node_defender_src, self.number_sx)
            self.print_image_sx(path_sx, 0)
            self.print_image_dx(path_dx, 1)

        # Images path
        self.path_sx = '../output/output_p{}_graph.png'.format(self.number_sx)
        self.path_dx = '../output/output_q{}_graph.png'.format(self.number_dx)

        # To insert text boxes
        self.insert_input()

        # Buttons to change image
        self.print_input_button_sx = tk.Button(self.master, text="Muovi", command=self.change_image_sx)
        self.print_input_button_sx.grid(row=3, column=0, padx=3, pady=10)
        self.print_input_button_dx = tk.Button(self.master, text="Muovi", command=self.change_image_dx)
        self.print_input_button_dx.grid(row=3, column=1, padx=3, pady=10)

        # Button to switch
        self.print_input_button_switch = tk.Button(self.master, text="Switch", command=self.switch)
        self.print_input_button_switch.grid(row=2, column=0, pady=10, columnspan=2)

        # Button to declare win
        self.print_input_button_win_sx = tk.Button(self.master, text="Vittoria", command=self.win_sx)
        self.print_input_button_win_sx.grid(row=2, column=0, padx=3, pady=10, sticky='W')
        self.print_input_button_win_dx = tk.Button(self.master, text="Vittoria", command=self.win_dx)
        self.print_input_button_win_dx.grid(row=2, column=1, padx=3, pady=10, sticky='E')

        # Button to close window
        print_input_button_quit = tk.Button(self.frame, text="Termina partita", command=self.close_window)
        print_input_button_quit.grid(row=0, column=2, padx=4, pady=5)

        # Button to delete a game
        print_input_button_delete = tk.Button(self.frame, text="Elimina partita salvata", command=self.delete)
        print_input_button_delete.grid(row=0, column=3, padx=4, pady=5)

        # To save a game
        self.butnew_save("Salva partita", Bisimulami)

        if first_exception == 1:
            self.write_message_sx("Non ci sono partite salvate. Ne abbiamo creata una nuova")

    def delete(self):
        """
        To delete a saved match
        """
        self.my_game.dbctrl.delete_saved_match(self.id)
        self.close_window()

    def view_start_image(self, graph_sx, graph_dx):
        """
        To view image at the beginning of the match
        :param graph_sx: path for the image on the left
        :param graph_dx: path for the image on the right
        """
        self.view_image_sx(graph_sx)
        self.view_image_dx(graph_dx)

    def view_image_sx(self, graph_sx):
        """
        To view image on the left of the screen
        :param graph_sx: path for the image on the left
        """
        self.master.img_sx = self.print_image(graph_sx)
        self.master.panel_sx = tk.Label(self.master.frame_sx, image=self.master.img)
        self.master.panel_sx.grid(row=1, column=0, padx=0)

    def view_image_dx(self, graph_dx):
        """
        To view image on the right of the screen
        :param graph_dx: path for the image on the right
        """
        self.master.img_dx = self.print_image(graph_dx)
        self.master.panel_dx = tk.Label(self.master.frame_dx, image=self.master.img)
        self.master.panel_dx.grid(row=1, column=1, padx=0, sticky='WE')

    def print_image(self, graph):
        """
        To set settings to view image
        :param graph:
        """
        self.master.img = Image.open(graph)
        self.master.img = self.master.img.resize((330, 400), Image.ANTIALIAS)
        self.master.img = ImageTk.PhotoImage(self.master.img)
        return self.master.img

    def insert_input(self):
        """
        To manage user inputs
        """
        self.insert_input_sx()
        self.insert_input_dx()

    def insert_input_sx(self):
        """
        To manage user input on the left
        """
        self.master.label_striker = tk.Label(self.master, text="Attaccante", foreground='black', background='#fb0')
        self.master.label_striker.grid(row=3, column=0, padx=3, pady=10, sticky='W')
        self.master.label_src_sx = tk.Label(self.master, text="Nodo sorgente", foreground='black', background='white')
        self.master.label_src_sx.grid(row=4, column=0, padx=3, pady=3, sticky='W')
        self.master.text_src_sx = tk.Entry(self.master)
        self.master.text_src_sx.grid(row=4, column=0, padx=4, pady=3)
        self.master.label_dst_sx = tk.Label(self.master, text="Nodo destinazione", foreground='black',
                                            background='white')
        self.master.label_dst_sx.grid(row=5, column=0, padx=3, pady=4, sticky='W')
        self.master.text_dst_sx = tk.Entry(self.master)
        self.master.text_dst_sx.grid(row=5, column=0, padx=4, pady=4)
        # Label that appears in case of exceptions
        self.master.label_exc_sx = tk.Label(self.master, text="", foreground='red', background='white')

    def insert_input_dx(self):
        """
        To manage user input on the right
        """
        self.master.label_defender = tk.Label(self.master, text="Difensore", foreground='black', background='#fb0')
        self.master.label_defender.grid(row=3, column=1, padx=3, pady=10, sticky='W')
        self.master.label_src_dx = tk.Label(self.master, text="Nodo sorgente", foreground='black', background='white')
        self.master.label_src_dx.grid(row=4, column=1, padx=3, pady=3, sticky='W')
        self.master.text_src_dx = tk.Entry(self.master)
        self.master.text_src_dx.grid(row=4, column=1, padx=4, pady=3)
        self.master.label_dst_dx = tk.Label(self.master, text="Nodo destinazione", foreground='black',
                                            background='white')
        self.master.label_dst_dx.grid(row=5, column=1, padx=3, pady=4, sticky='W')
        self.master.text_dst_dx = tk.Entry(self.master)
        self.master.text_dst_dx.grid(row=5, column=1, padx=4, pady=4)
        # Label che compare in caso di eccezioni
        self.master.label_exc_dx = tk.Label(self.master, text="", foreground='red', background='white')

    def check_text_sx(self):
        """
        To check if left input is ok
        """
        # If inputs aren't null
        if self.master.text_src_sx.get() and self.master.text_dst_sx.get():
            source = self.master.text_src_sx.get()
            destination = self.master.text_dst_sx.get()
            return source, destination
        else:
            message = "Devi inserire un valore in entrambi le caselle"
            self.write_message_sx(message)
            self.clean_all_sx()
            return -1, -1

    def check_text_dx(self):
        """
        To check if right input is ok
        """
        # If inputs aren't null
        if self.master.text_src_dx.get() and self.master.text_dst_dx.get():
            source = self.master.text_src_dx.get()
            destination = self.master.text_dst_dx.get()
            return source, destination
        else:
            message = "Devi inserire un valore in entrambi le caselle"
            self.write_message_dx(message)
            self.clean_all_dx()
            return -1, -1

    def found_error(self, player, source, destination, number):
        """
        To find all types of errors
        :param player: striker or defender
        :param source: source node
        :param destination: destination node
        :param number: number for the counter
        :return: a message for the user
        """
        result = self.my_game.new_move(player, source, destination, number)
        # If the source node and the destination node are not present in the graph
        if result == 0:
            message = "Il nodo {} e il nodo {} non sono presenti nel grafo".format(source, destination)
            return message

        #  If the source node is not present in the graph
        elif result == -1:
            message = "Non puoi iniziare la mossa nel nodo {}".format(source)
            return message

        #  If the destination node is not present in the graph
        elif result == -2:
            message = "Non puoi terminare la mossa nel nodo {}".format(destination)
            return message

        # If the inserted arch does not exist
        elif result == -3:
            message = "L'arco {}->{} non esiste".format(source, destination)
            return message

        # If the defender moves with a different move than the striker
        elif result == -4:
            message = "Devi usare un arco con la stessa label dell'attaccante"
            return message

        # If the source node and the destination node are present in the graph
        else:
            return 1

    def write_message_sx(self, message):
        """
        To print left error message
        :param message: message for the user
        """
        self.master.label_exc_sx.config(text=message)
        self.master.label_exc_sx.grid(row=6, column=0, padx=3, pady=10, sticky='W')

    def write_message_dx(self, message):
        """
        To print right error message
        :param message: message for the user
        """
        self.master.label_exc_dx.config(text=message)
        self.master.label_exc_dx.grid(row=6, column=1, padx=3, pady=10, sticky='W')

    def print_image_sx(self, path, my_column):
        """
        To view left image
        :param path: path of the image
        :param my_column: 0 for the left image and 1 for the right one
        """
        self.master.graph_sx = path
        self.master.img_sx = Image.open(self.master.graph_sx)
        #                      width, height
        self.master.img_sx = self.master.img_sx.resize((330, 400), Image.ANTIALIAS)
        self.master.img_sx = ImageTk.PhotoImage(self.master.img_sx)
        self.master.panel_sx = tk.Label(self.master.frame_sx, image=self.master.img_sx)
        self.master.panel_sx.grid(row=1, column=my_column)

    def print_image_dx(self, path, my_column):
        """
        To view right image
        :param path: path of the image
        :param my_column: 0 for the left image and 1 for the right one
        """
        self.master.graph_dx = path
        self.master.img_dx = Image.open(self.master.graph_dx)
        #                      width, height
        self.master.img_dx = self.master.img_dx.resize((330, 400), Image.ANTIALIAS)
        self.master.img_dx = ImageTk.PhotoImage(self.master.img_dx)
        self.master.panel_dx = tk.Label(self.master.frame_dx, image=self.master.img_dx)
        self.master.panel_dx.grid(row=1, column=my_column)

    def change_image_sx(self):
        '''
        To change left image
        '''
        # If the game has been saved, de-color the nodes before changing the image
        if self.node_striker_src.__contains__(''):
            self.my_game.clean_specific_code(self.node_striker_src, self.node_striker_dst)
        source, destination = self.check_text_sx()

        # To check if text boxes are not empty
        if source == -1 and destination == -1:
            return

        # To check for node insertion errors
        message = self.found_error('a', source, destination, self.number_sx)
        if message != 1:
            self.write_message_sx(message)

        # If everything works ok
        else:
            self.node_striker_src = source
            self.node_striker_dst = destination
            self.player = 'a'
            self.check_switch_player = 'd'
            source = source[0]
            # If source is in P graph
            if (source == 'p'):
                self.path_sx = '../output/output_p{}_graph.png'.format(self.number_sx)
                self.number_p = self.number_p + 1
            else:
                self.path_sx = '../output/output_q{}_graph.png'.format(self.number_sx)
                self.number_q = self.number_q + 1

            self.number_sx = self.number_sx + 1
            self.print_image_sx(self.path_sx, 0)
            self.master.label_exc_sx.grid_forget()
        self.clean_all_sx()

    def change_image_dx(self):
        """
        To change right image
        """
        source, destination = self.check_text_dx()
        # To check if text boxes are not empty
        if source == -1 and destination == -1:
            return

        # To check for node insertion errors
        message = self.found_error('d', source, destination, self.number_dx)

        if message != 1:
            self.write_message_dx(message)

        # If everything works ok
        else:
            self.node_defender_src = source
            self.node_defender_dst = destination
            source = source[0]
            if (source == 'p'):
                self.path_dx = '../output/output_p{}_graph.png'.format(self.number_dx)
                self.number_p = self.number_p + 1

            else:
                self.path_dx = '../output/output_q{}_graph.png'.format(self.number_dx)
                self.number_q = self.number_q + 1

            self.number_dx = self.number_dx + 1
            self.print_image_dx(self.path_dx, 1)
            self.master.label_exc_dx.grid_forget()

        # If the striker makede an exception, delete that
        if self.player.__contains__('a'):
            self.master.label_exc_sx.grid_forget()

        self.player = 'd'
        self.check_switch_player = 'a'
        self.clean_all_dx()

    def switch(self):
        """
        To switch fiels
        """
        # If the turn is of the striker
        if self.check_switch_player == 'a':
            # If it is a switch on the first turn
            if self.number_p == 0 and self.number_q == 0:
                if "p0" in self.path_dx:
                    self.path_sx = '../output/output_p0_graph.png'
                    self.path_dx = '../output/output_q0_graph.png'

                else:
                    self.path_sx = '../output/output_q0_graph.png'
                    self.path_dx = '../output/output_p0_graph.png'

            # If there was only a switch from the defender
            elif self.number_p == 0 and self.number_q == 1:
                if "p0" in self.path_dx:
                    self.path_dx = self.path_sx
                    self.path_sx = '../output/output_p0_graph.png'
                else:
                    self.path_sx = self.path_dx
                    self.path_dx = '../output/output_p0_graph.png'

            # If there was only a switch from the striker
            elif self.number_p == 1 and self.number_q == 0:
                if "q0" in self.path_sx:
                    self.path_sx = self.path_dx
                    self.path_dx = '../output/output_q0_graph.png'
                else:
                    self.path_dx = self.path_sx
                    self.path_sx = '../output/output_q0_graph.png'

            else:
                temp = self.path_sx
                self.path_sx = self.path_dx
                self.path_dx = temp

            self.print_image_sx(self.path_sx, 0)
            self.print_image_dx(self.path_dx, 1)

        else:
            self.write_message_dx("Non puoi fare lo switch a questo punto della partita")

    def clean_all_sx(self):
        """
        To clean the image (all white)
        """
        self.my_game.clean_code()

        # To delete the input just entered
        self.master.text_src_sx.delete(0, tk.END)
        self.master.text_dst_sx.delete(0, tk.END)

    def clean_all_dx(self):
        """
        To clean the image (all white)
        """
        self.my_game.clean_code()

        # To delete the input just entered
        self.master.text_src_dx.delete(0, tk.END)
        self.master.text_dst_dx.delete(0, tk.END)

    def win_sx(self):
        """
        To display the striker's victory
        :return:
        """
        self.master.panel_sx.grid_forget()
        self.master.img_win_striker = Image.open("../clipart/win.jpg")
        self.master.img_win_striker = self.master.img_win_striker.resize((330, 400), Image.ANTIALIAS)
        self.master.img_win_striker = ImageTk.PhotoImage(self.master.img_win_striker)
        self.master.panel_sx = tk.Label(self.master.frame_sx, image=self.master.img_win_striker)
        self.master.panel_sx.grid(row=1, column=0)

        self.master.panel_dx.grid_forget()
        self.master.img_defeat_defender = Image.open("../clipart/defeat_defender.jpg")
        self.master.img_defeat_defender = self.master.img_defeat_defender.resize((330, 400), Image.ANTIALIAS)
        self.master.img_defeat_defender = ImageTk.PhotoImage(self.master.img_defeat_defender)
        self.master.panel_dx = tk.Label(self.master.frame_dx, image=self.master.img_defeat_defender)
        self.master.panel_dx.grid(row=1, column=1)

        self.clean_input_win_button_sx()

    def win_dx(self):
        """
        To display the defender's victory
        """
        self.master.panel_sx.grid_forget()
        self.master.img_defeat_striker = Image.open("../clipart/defeat_striker.jpg")
        self.master.img_defeat_striker = self.master.img_defeat_striker.resize((330, 400), Image.ANTIALIAS)
        self.master.img_defeat_striker = ImageTk.PhotoImage(self.master.img_defeat_striker)
        self.master.panel_sx = tk.Label(self.master.frame_sx, image=self.master.img_defeat_striker)
        self.master.panel_sx.grid(row=1, column=0)

        self.master.panel_dx.grid_forget()
        self.master.img_win_defender = Image.open("../clipart/win.jpg")
        self.master.img_win_defender = self.master.img_win_defender.resize((330, 400), Image.ANTIALIAS)
        self.master.img_win_defender = ImageTk.PhotoImage(self.master.img_win_defender)
        self.master.panel_dx = tk.Label(self.master.frame_dx, image=self.master.img_win_defender)
        self.master.panel_dx.grid(row=1, column=1)

        self.clean_input_win_button_dx()

    def clean_input_win_button_sx(self):
        """
        To hide text boxes and labels that are not necessary for the visualization of the victory/defeat
        """
        self.print_input_button_win_sx.grid_forget()
        self.print_input_button_win_dx.grid_forget()
        self.print_input_button_switch.grid_forget()
        self.print_input_button_sx.grid_forget()
        self.print_input_button_dx.grid_forget()
        # self.master.label_striker.grid_forget()
        self.master.label_src_sx.grid_forget()
        self.master.label_dst_sx.grid_forget()
        self.master.text_src_sx.grid_forget()
        self.master.text_dst_sx.grid_forget()
        self.master.label_exc_sx.grid_forget()
        # self.master.label_defender.grid_forget()
        self.master.label_src_dx.grid_forget()
        self.master.label_dst_dx.grid_forget()
        self.master.text_src_dx.grid_forget()
        self.master.text_dst_dx.grid_forget()
        self.master.label_exc_dx.grid_forget()

    def clean_input_win_button_dx(self):
        """
        To hide text boxes and labels that are not necessary for the visualization of the victory/defeat
        """
        self.print_input_button_win_sx.grid_forget()
        self.print_input_button_win_dx.grid_forget()
        self.print_input_button_switch.grid_forget()
        self.print_input_button_sx.grid_forget()
        self.print_input_button_dx.grid_forget()
        # self.master.label_striker.grid_forget()
        self.master.label_src_sx.grid_forget()
        self.master.label_dst_sx.grid_forget()
        self.master.text_src_sx.grid_forget()
        self.master.text_dst_sx.grid_forget()
        self.master.label_exc_sx.grid_forget()
        # self.master.label_defender.grid_forget()
        self.master.label_src_dx.grid_forget()
        self.master.label_dst_dx.grid_forget()
        self.master.text_src_dx.grid_forget()
        self.master.text_dst_dx.grid_forget()
        self.master.label_exc_dx.grid_forget()

    def butnew(self, text, _class):
        """
        :param text: the text of the message
        :param _class: the class to create
        """
        tk.Button(self.frame, text=text, command=lambda: self.new_window(_class)).grid(row=0, column=0, padx=3, pady=5)

    def butnew_save(self, text, _class):
        """
        :param text: the text of the message
        :param _class: the class to create
        """
        tk.Button(self.frame, text=text, command=lambda: self.new_window_save(_class)).grid(row=0, column=1,
                                                                                            padx=4, pady=5)

    def new_window(self, _class):
        """
        :param text: the text of the message
        :param _class: the class to create
        """
        self.new = tk.Toplevel(self.master)
        _class(self.new)

    def new_window_save(self, _class):
        """
        :param text: the text of the message
        :param _class: the class to create
        """
        # If the striker tries to save the match
        if self.player.__contains__('a'):
            self.write_message_sx("Per salvare la partita aspetta che anche il difensore termini la mossa")

        else:
            self.my_game.dbctrl.saveMatch(self.id, self.player, self.node_striker_src,
                                          self.node_striker_dst, self.node_defender_src, self.node_defender_dst,
                                          self.number_sx, self.number_dx, self.number_p, self.number_q)
            self.close_window()

    def close_window(self):
        """
        To destroy the window
        """
        self.master.destroy()
