import tkinter as tk
from PIL import ImageTk
from PIL import Image
from src.interfaces.Bisimulami import Bisimulami


# Class to choose level of difficulty
class Choose_Difficulty:

    def __init__(self, master):
        """
        Create an instance of Choose_Difficulty
        :param master: tk instance
        """
        self.master = master
        self.master.title("Difficoltà")
        self.master.geometry("" '+500+5')
        self.master.resizable(False, False)
        self.master.configure(background='white')

        # Button to close window
        print_input_button_quit = tk.Button(self.master, text="Esci", command=self.close_window)
        print_input_button_quit.grid(row=2, pady=5, columnspan=3)

        # To visualize image
        self.master.img = ImageTk.PhotoImage(Image.open("../clipart/difficulty.png"))
        self.master.panel = tk.Label(self.master, image=self.master.img)
        self.master.panel.grid(row=1, columnspan=3, padx=5, pady=0)

        self.butnew_high("Alta", Bisimulami)
        self.butnew_medium("Media", Bisimulami)
        self.butnew_low("Bassa", Bisimulami)

    def butnew_high(self, text, _class):
        """
        Button to choose high difficulty
        :param text: text of the button
        :param _class: class to create
        """
        tk.Button(self.master, text=text, command=lambda: self.new_window_difficulty(_class, text)).grid(row=0,
                                                                                                         column=0,
                                                                                                         padx=5,
                                                                                                         pady=10)

    def butnew_medium(self, text, _class):
        """
        Button to choose medium difficulty
        :param text: text of the button
        :param _class: class to create
        """
        tk.Button(self.master, text=text, command=lambda: self.new_window_difficulty(_class, text)).grid(row=0,
                                                                                                         column=1,
                                                                                                         padx=5,
                                                                                                         pady=10)

    def butnew_low(self, text, _class):
        """
        Button to choose low difficulty
        :param text: text of the button
        :param _class: class to create
        """
        tk.Button(self.master, text=text, command=lambda: self.new_window_difficulty(_class, text)).grid(row=0,
                                                                                                         column=2,
                                                                                                         padx=5,
                                                                                                         pady=10)

    def new_window_difficulty(self, _class, text):
        """
        Window to choose high difficulty
        :param text: text of the button
        :param _class: class to create
        """
        print("testo {}".format(text))
        self.new = tk.Toplevel(self.master)
        # To create a new class
        _class(self.new, text, '', '', '', '', 1, 1, 0, 0)

    def close_window(self):
        """
        To destroy the window
        """
        self.master.destroy()