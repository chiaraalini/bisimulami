import glob
import os
import tkinter as tk
from PIL import ImageTk
from PIL import Image
import sys
sys.path.append("..")
from src.GameManager import Game
from src.interfaces.Bisimulami import Bisimulami
from src.interfaces.Choosedifficulty import Choose_Difficulty


class Start:

    def __init__(self, master):
        """
        To initialize Start instance
        :param master:
        """
        self.checkDirectory()
        # To create view
        self.master = master
        # Size and position
        self.master.geometry('' '+500+5')
        self.master.title("Bisimulami!")
        self.master.configure(background='white')
        self.master.resizable(False, False)

        self.master.label = tk.Label(self.master, text="Benvenuto in Bisimulami!", foreground='red',
                                     background='#fb0',
                                     font=("Comic Sans MS", 14, ""))
        self.master.label.grid(row=0, pady=10, columnspan=3)

        # Create buttons
        self.butnew("Nuova partita", Bisimulami)
        self.butnew_choose_difficulty("Scegli difficoltà", Choose_Difficulty)
        self.butnew_resume("Riprendi partita", Bisimulami)

        # To visualize images
        self.master.img = Image.open("../clipart/graph.jpg")
        self.master.img = self.master.img.resize((286, 161), Image.ANTIALIAS)
        self.master.img = ImageTk.PhotoImage(self.master.img)
        self.master.panel = tk.Label(self.master, image=self.master.img)
        self.master.panel.grid(row=1, columnspan=2, padx=7, pady=10)

        # To close window
        print_input_button_quit = tk.Button(self.master, text="Esci", command=self.close_window)
        print_input_button_quit.grid(row=4, pady=5, columnspan=3)

    def butnew(self, text, _class):
        """
        Button to start a new match
        :param text: tex of the button
        :param _class: instance of a new class
        """
        tk.Button(self.master, text=text, command=lambda: self.new_window(_class)).grid(row=2, column=0, padx=7,
                                                                                        pady=10,
                                                                                        sticky='W')

    def butnew_choose_difficulty(self, text, _class):
        """
        Button to choose the difficuty of the match
        :param text: tex of the button
        :param _class: instance of a new class
        """
        tk.Button(self.master, text=text, command=lambda: self.new_window_choose_difficulty(_class)).grid(row=2,
                                                                                                          column=1,
                                                                                                          padx=7,
                                                                                                          rowspan=2,
                                                                                                          sticky='E')

    def butnew_resume(self, text, _class):
        """
        Button to resume a saved match
        :param text: tex of the button
        :param _class: instance of a new class
        """
        tk.Button(self.master, text=text, command=lambda: self.new_window_resume(_class)).grid(row=3, column=0, padx=7,
                                                                                               pady=10, sticky='W')

    def new_window(self, _class):
        """
        Window to start the match
        :param _class:
        """
        self.new = tk.Toplevel(self.master)
        # To create a new class
        _class(self.new, '', '', '', '', '', 1, 1, 0, 0)


    def new_window_choose_difficulty(self, _class):
        """
        Window to choose the difficuty of the match
        :param _class:
        """
        self.new = tk.Toplevel(self.master)
        _class(self.new)

    def new_window_resume(self, _class):
        """
        Window to resume a saved match
        :param _class:
        """
        self.my_game = Game(-1)
        val = self.my_game.val
        # If a match is found
        if val is None:
            self.new = tk.Toplevel(self.master)
            _class(self.new, 'e', '', '', '', '', 1, 1, 0, 0)
        else:
            self.new = tk.Toplevel(self.master)
            _class(self.new, val[2], val[3], val[4], val[5], val[6], val[7], val[8], val[9], val[10])

    def checkDirectory(self):
        """
        To delete old files in output directory
        """
        path = "../output"

        if not os.path.exists(path):
            os.makedirs(path)
        files = glob.glob('../output/*')
        for f in files:
            os.remove(f)

    def close_window(self):
        self.master.destroy()


# Start GUI
root = tk.Tk()
app = Start(root)
root.mainloop()
