import tkinter as tk


class Instructions:

    def __init__(self, master):
        """
        To initialize Instruction instance
        :param master: tk instance
        """
        self.master = master
        self.master.geometry("" '+400+5')
        self.master.resizable(False, False)
        self.frame = tk.Frame(self.master)
        self.frame.pack()

        self.master.title("Instructions")
        self.master.geometry("490x400")
        self.master['bg'] = '#fb0'

        txtarea = tk.Text(self.master, width=100, height=200)
        txtarea.pack(pady=20)

        tf = open('../instructions/Instructions.txt', "r")
        data = tf.read()
        txtarea.insert(tk.END, data)
        tf.close()
