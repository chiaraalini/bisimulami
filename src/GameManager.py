import os
import pydot
from src.DBConnector import DBConnector

class Game:

    def __init__(self, start_number = 1):
        '''
        To initialize Game instance
        :param start_number: to call different database function
        '''

        self.dbctrl = DBConnector()
        # Choose difficulty for the new match


        # Start saved match
        if start_number == -1:
            self.val = self.dbctrl.resumeMatch()
            self.dbctrl.extract()

        # If difficulty isn't chosen
        elif (type(start_number) == int):
            self.dbctrl.extract()

        else:
            self.dbctrl.extract(start_number)
            start_number = 1

        self.dots = self.dbctrl.stringaDOT1 + self.dbctrl.stringaDOT2
        self.label_strk = ''
        self.label_def = ''

        with open("../graphs.dot", "w") as f:
            f.write(self.dots)

        with open("../graphs.dot", "r") as file:
            self.text = file.readlines()

        # Start a new no saved match
        if start_number == 1:
            self.start()


    def start(self):
        '''
        To set correctly parameter for the first match
        '''

        # If output directory doesnt' exist
        path = "../output"
        if not os.path.exists(path):
            os.makedirs(path)

        graphs = pydot.graph_from_dot_file('../graphs.dot')
        graph1 = graphs[0]  # graph P
        graph2 = graphs[1]  # graph Q
        nome_p = '{}/output_p0_graph.png'.format(path)
        nome_q = '{}/output_q0_graph.png'.format(path)
        graph1.write_png(nome_p)
        graph2.write_png(nome_q)


    def new_move(self, player, source, destination, number):
        '''
        To check the new move
        :param player: striker or defender
        :param source: source node
        :param destination: destination node
        :param number: move number
        '''

        # If user inserts the same letter without number
        if (source == 'p' and destination == 'p') or (source == 'q' and destination == 'q'):
            self.clean_specific_code(source, destination)
            return 0

        # If user inserts letter without numbers in source node
        if source == 'p' or source == 'q':
            self.clean_specific_code(source, destination)
            return -1

        # If user inserts letter without numbers in destination node
        if destination == 'p' or destination == 'q':
            self.clean_specific_code(source, destination)
            return -2

        #If user makes an epsilon move
        if source == destination and player == 'd' and self.label_strk == 'tau':
            self.color_only_one_node(source, number)
            return 1

        found_src, found_dest, found_arch = self.find_nodes(player, source, destination)
        result = self.check_nodes(player, source, destination, number, found_src, found_dest, found_arch)
        return result


    def color_only_one_node(self, node, number):
        '''
        To manage defender move on the same node
        :param node: node
        :param number: number of the move
        '''

        i = 0
        keyword = 'fillcolor=white'
        replacement = 'fillcolor=red'

        while i < len(self.text):
            # If node is in the graph
            if node in self.text[i]:
                self.text[i] = self.text[i].replace(keyword, replacement)
            i += 1

        with open("../graphs.dot", "w") as file:
            file.writelines(self.text)
        self.view_graph(node, number)


    def find_nodes(self, player, source, destination):
        '''
        To find two nodes of the move
        :param player: striker or defender
        :param source: source node
        :param destination: destination node
        :return: values to see if both nodes are found or only one of them or arch is found
        '''

        i = 0
        keyword_src = 'fillcolor=white'
        replacement_src = 'fillcolor=green'
        keyword_dest = 'fillcolor=white'
        replacement_dest = 'fillcolor=red'
        arch = "{}->{}".format(source, destination)
        found_src = 0
        found_dest = 0
        found_arch = 0

        while i < len(self.text):
            # If all nodes are found
            if found_src == 1 and found_dest == 1:
                # If arch is in the graph
                if arch in self.text[i]:
                    found_arch = 1
                    self.set_label(player, self.text[i], arch)
                    break
            else:
                # If source node is found
                if source in self.text[i]:
                    self.text[i] = self.text[i].replace(keyword_src, replacement_src)
                    found_src = 1
                # If destination node is found
                if destination in self.text[i]:
                    self.text[i] = self.text[i].replace(keyword_dest, replacement_dest)
                    found_dest = 1
            i += 1
        return found_src, found_dest, found_arch


    def set_label(self, player, string, arch):
        '''
        To save correctly the label in the .dot file after the decomposition
        :param player: striker or defender
        :param string: text of the label
        :param arch: arch value
        '''

        # If the striker moves
        if player.__contains__('a'):
            self.label_strk = string
            self.label_strk = self.label_strk.replace(' {} [label = '.format(arch), '')
            self.label_strk = self.label_strk.replace('];\n', '')
        else:
            self.label_def = string
            self.label_def = self.label_def.replace(' {} [label = '.format(arch), '')
            self.label_def = self.label_def.replace('];\n', '')


    def check_nodes(self, player, source, destination, number, found_src, found_dest, found_arch):
        '''
        To define all cases found
        :param player: striker or defender
        :param source: source node
        :param destination: destination node
        :param number: number of move
        :param found_src: value to see if source node is found
        :param found_dest: value to see if source node is found
        :param found_arch: value to see if arch is found
        :return: different values according to the situation
        '''

        # If source and definition node aren't in graph
        if found_src == 0 and found_dest == 0:
            self.clean_specific_code(source, destination)
            return 0

        # If source node isn't in graph
        elif found_src == 0:
            self.clean_specific_code(source, destination)
            return -1

        # If destination node isn't in graph
        elif found_dest == 0:
            self.clean_specific_code(source, destination)
            return -2

        # If arch isn't in graph
        elif found_arch == 0:
            return -3

        else:
            # If the player is defender
            if player.__contains__('d'):
                # If striker uses tau arch
                if self.label_strk.__contains__("tau"):
                    # If defensor uses weak transition
                    if self.label_strk != self.label_def and source != destination:
                        return -4
                else:
                    print(self.label_strk)
                    print(self.label_def)
                    if self.label_strk != self.label_def:
                        return -4

            with open("../graphs.dot", "w") as file:
                file.writelines(self.text)
            self.view_graph(source, number)
            return 1


    def clean_specific_code(self, source, destination):
        '''
        To clean only one colored node
        :param source: source node
        :param destination:destination node
        '''

        i = 0
        keyword_1 = 'fillcolor=green'
        keyword_2 = 'fillcolor=red'
        replacement = 'fillcolor=white'
        while i < len(self.text):
            if keyword_1 in self.text[i] and (source in self.text[i] or destination in self.text[i]):
                self.text[i] = self.text[i].replace(keyword_1, replacement)
            if keyword_2 in self.text[i] and (source in self.text[i] or destination in self.text[i]):
                self.text[i] = self.text[i].replace(keyword_2, replacement)
            i += 1
        with open("../graphs.dot", "w") as file:
            file.writelines(self.text)


    def clean_code(self):
        '''
        To clean all colors in the graph
        '''

        i = 0
        keyword_1 = 'fillcolor=green'
        keyword_2 = 'fillcolor=red'
        replacement = 'fillcolor=white'
        while i < len(self.text):
            if keyword_1 in self.text[i]:
                self.text[i] = self.text[i].replace(keyword_1, replacement)
            if keyword_2 in self.text[i]:
                self.text[i] = self.text[i].replace(keyword_2, replacement)
            i += 1
        with open("../graphs.dot", "w") as file:
            file.writelines(self.text)


    def view_graph(self, source, number):
        '''
        To visualize the graph
        :param source: source node
        :param number: number of move
        '''

        graphs = pydot.graph_from_dot_file('../graphs.dot')
        graph1 = graphs[0]  # graph P
        graph2 = graphs[1]  # graph Q

        source = source[0]

        # If source node is in the P graph
        if (source == 'p'):
            nome_p = '../output/output_p{}_graph.png'.format(number)
            graph1.write_png(nome_p)
        else:
            nome_q = '../output/output_q{}_graph.png'.format(number)
            graph2.write_png(nome_q)
