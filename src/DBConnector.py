import mysql.connector
#import sqlite3

import json


class DBConnector:

    def establishConnection(self, first=False):
        '''
        Establish the connection with the database.
        :param first: to be set to True in the case that the database doesn't exist yet.
        :return: the connection object
        '''

        with open("../conf/configuration.json") as conf_file:
            conf_data = conf_file.read()

        conf_json = json.loads(conf_data)

        if conf_json['dbms'] == 'mysql':
            if first:
                del(conf_json["dbms_configuration"]["database"])

            db = mysql.connector.connect(**conf_json["dbms_configuration"])
            return db


    def extract(self, difficoltà='Media'):
        '''
        Extract the pair of graphs from the database.
        :param difficoltà: The desired difficulty that the extracted pair must match.
        '''

        # Database connection
        db = self.establishConnection()

        # Record extraction
        cursor = db.cursor()
        sql = "SELECT DOTString1, DOTString2, id FROM coppie_di_grafi WHERE Difficoltà = '{0}'".format(difficoltà)
        cursor.execute(sql)

        result = cursor.fetchone()
        self.stringaDOT1 = result[0]
        self.stringaDOT2 = result[1]
        self.id = result[2]
        ### return stringaDOT1, stringaDOT2


    def saveMatch(self, id_coppia, player, nodo_attacante_src, nodo_attacante_dst, nodo_difensore_src, nodo_difensore_dst,
                  number_sx, number_dx, number_p, number_q):
        '''
        Save the ongoing match.
        '''

        # Database connection
        db = self.establishConnection()

        # Match insertion
        sql = "INSERT INTO partite (id_coppia, player, nodo_attacante_src, nodo_attacante_dst, nodo_difensore_src, " \
              "nodo_difensore_dst, number_sx, number_dx, number_p, number_q) " \
              "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (id_coppia, player, nodo_attacante_src, nodo_attacante_dst, nodo_difensore_src, nodo_difensore_dst, number_sx,
               number_dx, number_p, number_q)

        cursor = db.cursor()
        cursor.execute(sql, val)
        db.commit()
        print("Partita salvata.")


    def resumeMatch(self):
        '''
        Resume a match.
        :return: the match left uncompleted.
        '''

        # Database connection
        db = self.establishConnection()

        # Match extraction
        cursor = db.cursor()
        sql = "SELECT * FROM partite"
        cursor.execute(sql)

        return cursor.fetchone()


    def delete_saved_match(self, id):
        '''
        Delete the pending match.
        :param id: identifier of the match to delete.
        '''

        # Database connection
        db = self.establishConnection()

        cursor = db.cursor()
        sql = "DELETE FROM partite WHERE id_coppia = {}".format(id)
        cursor.execute(sql)
        db.commit()

        print("partita eliminata")


# USAGE
# It is necessary to execute this script as main to create the database and populate it.
# A second execution will drop the database (if it exists) so be careful.
if __name__ == "__main__":

    # Database creation (and if need be deletion)
    first = True
    DB = DBConnector()
    db = DB.establishConnection(first)

    cursor = db.cursor()
    cursor.execute("DROP DATABASE IF EXISTS bisimulami")
    cursor.execute("CREATE DATABASE bisimulami")

    # Database connection
    db = DB.establishConnection()

    # Tables creation
    cursor = db.cursor()
    cursor.execute("CREATE TABLE coppie_di_grafi ("
                   "id INT AUTO_INCREMENT PRIMARY KEY, "
                   "DOTString1 VARCHAR(1000), "
                   "DOTString2 VARCHAR(1000), "
                   "Bisimiglianza BOOLEAN, "
                   "Già_giocato BOOLEAN, "
                   "Vinto_allUltima BOOLEAN, "
                   "ProvenienzaData DATE, "
                   "ProvenienzaTipo ENUM('Esame', 'Esercizio', 'Esercitazione'), "
                   "Difficoltà ENUM('Bassa', 'Media', 'Alta'))")

    cursor.execute("CREATE TABLE partite ("
                   "id INT AUTO_INCREMENT PRIMARY KEY, "
                   "id_coppia INT, "
                   "player VARCHAR(10), "
                   "nodo_attacante_src VARCHAR(10), "
                   "nodo_attacante_dst VARCHAR(10), "
                   "nodo_difensore_src VARCHAR(10), "    
                   "nodo_difensore_dst VARCHAR(10), "
                   "number_sx INT, "
                   "number_dx INT, "
                   "number_p INT, "
                   "number_q INT)")

    # Columns showing
    # cursor.execute("SHOW COLUMNS IN coppie_di_grafi")
    # for x in cursor:
    #    print(x)

    # DB population
    sql = "INSERT INTO coppie_di_grafi (DOTString1, DOTString2, Bisimiglianza, Già_giocato, Vinto_allUltima, " \
          "ProvenienzaData, ProvenienzaTipo, Difficoltà) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    val = [
        (
            'digraph d_1 {\n p1 [label="p1", style=filled, fillcolor=white];\n p2 [label="p2", style=filled, fillcolor=white];\n '
            'p3 [label="p3", style=filled, fillcolor=white];\n p4 [label="p4", style=filled, fillcolor=white];\n '
            'p5 [label="p5", style=filled, fillcolor=white];\n p6 [label="p6", style=filled, fillcolor=white];\n '
            'pNIL [label="pNIL", style=filled, fillcolor=white];\n p_NIL [label="p_NIL", style=filled, fillcolor=white];\n\n'
            ' p1->p2 [label = a];\n p2->p4 [label = tau];\n p2->p_NIL [label = b];\n p1->p3 [label = a];\n'
            ' p3->p6 [label = tau];\n p6->p_NIL [label = c];\n p3->p5 [label = tau];\n p5->p_NIL [label = b];\n p4->pNIL [label = c];\n'
            ' p3->p_NIL [label = b];\n}\n',
            '\ndigraph d_2 {\n q1 [label="q1", style=filled, fillcolor=white];\n q2 [label="q2", style=filled, fillcolor=white];\n '
            'q3 [label="q3", style=filled, fillcolor=white];\n q4 [label="q4", style=filled, fillcolor=white];\n '
            'q_NIL [label="q_NIL", style=filled, fillcolor=white];\n\n'
            ' q1->q2 [label = a];\n q2->q_NIL [label = c];\n q1->q3 [label = a];\n q3->q4 [label = tau];\n q4->q_NIL [label = c];\n '
            'q3->q_NIL [label = b];\n}', '1', '1', '1', '2019-11-29', 'Esercitazione', 'Bassa'),
        (
            'digraph d_1 {\n p1 [label="p1", style=filled, fillcolor=white];\n p2 [label="p2", style=filled, fillcolor=white];\n '
            'p3 [label="p3", style=filled, fillcolor=white];\n p4 [label="p4", style=filled, fillcolor=white];\n '
            'p_NIL [label="p_NIL", style=filled, fillcolor=white];\n\n'
            ' p1->p2 [label = c];\n p2->p4 [label = tau];\n p4->p_NIL [label = b];\n p2->p3 [label = tau];\n'
            ' p3->p_NIL [label = a];\n}\n',
            '\ndigraph d_2 {\n q1 [label="q1", style=filled, fillcolor=white];\n q2 [label="q2", style=filled, fillcolor=white];\n '
            'q3 [label="q3", style=filled, fillcolor=white];\n q4 [label="q4", style=filled, fillcolor=white];\n '
            'q5 [label="q5", style=filled, fillcolor=white];\n q_NIL [label="q_NIL", style=filled, fillcolor=white];\n\n'
            ' q1->q2 [label = c];\n q2->q_NIL [label = a];\n q4->q_NIL [label = b];\n q1->q3 [label = c];\n q3->q5 [label = tau];\n q5->q_NIL [label = a];\n '
            'q3->q_NIL [label = b];\n q2->q4 [label = tau];\n}', '1', '1', '1', '2020-9-10', 'Esame', 'Alta'),
        (
            'digraph d_1 {\n p1 [label="p1", style=filled, fillcolor=white];\n p2 [label="p2", style=filled, fillcolor=white];\n '
            'p3 [label="p3", style=filled, fillcolor=white];\n p4 [label="p4", style=filled, fillcolor=white];\n '
            'p_NIL [label="p_NIL", style=filled, fillcolor=white];\n\n'
            ' p1->p2 [label = a];\n p4->p_NIL [label = c];\n p3->p_NIL [label = b];\n p3->p4 [label = tau];\n'
            ' p2->p_NIL [label = c];\n p1->p3 [label = a];\n}\n',
            '\ndigraph d_2 {\n q1 [label="q1", style=filled, fillcolor=white];\n q2 [label="q2", style=filled, fillcolor=white];\n '
            'q3 [label="q3", style=filled, fillcolor=white];\n q4 [label="q4", style=filled, fillcolor=white];\n '
            'q5 [label="q5", style=filled, fillcolor=white];\n q_NIL [label="q_NIL", style=filled, fillcolor=white];\n\n'
            ' q1->q2 [label = a];\n q2->q_NIL [label = c];\n q1->q3 [label = a];\n q3->q4 [label = tau];\n q4->q_NIL [label = c];\n '
            'q3->q5 [label = b];\n q5->q_NIL [label = b];\n}', '1', '1', '1', '2018-9-6', 'Esame', 'Media')
    ]

    cursor.executemany(sql, val)
    db.commit()
    print(cursor.rowcount, "righe inserite.")
